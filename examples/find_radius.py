# Purpose: For a given start cell find the smallest radius that allows one to cover all grid cell.
# This is helpful as a too large radius artifically inflates the size of the cubulated grid.

# Written for the limited-area grids of ICON used in the Tricco introduction paper.

# Use example on mistral: /pf/b/b380459/conda-envs/Nawdex-Hackathon/bin/python3 find_radius.py R80000m 5570 200

# parse command line parameters
import sys
resol  = sys.argv[1]        # grid resolution, e.g., R80000m
start  = int(sys.argv[2])   # index of start cell
radius = int(sys.argv[3])   # maximum search radius

print('-----------------------------------------')
print('Working on ICON grid with resolution', resol)
print('Start cell   :', start)
print('Search radius:', radius)

import sys
sys.path.append('/pf/b/b380459/connected-components-3d/')
sys.path.append('/pf/b/b380459/BigDataClouds/tricco/')
import tricco

# gridfile including path
gridfile = '/work/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_'+resol+'.nc'

tricco.grid_functions.grid = tricco.prepare_grid(model='ICON', path='/', file=gridfile)

# perform the cubulation with diagnostic output via print_progress=True
# for each iteration, this prints to screen the number of new triangles for which cubic indices were computed 
cubulation = tricco.compute_cubulation(start_triangle=start, radius=radius, print_progress=True)
