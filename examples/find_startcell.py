# Purpose: Find the cell closest to a given latitude-longitude position.
# This is helpful to set the start cell of the cubulation routine.

# Written for the limited-area grids of ICON used in the Tricco introduction paper.

# Use example on mistral: /pf/b/b380459/conda-envs/Nawdex-Hackathon/bin/python3 find_startcell.py R80000m 51.5 19

# convert rad to deg
import numpy as np
rad2deg=180.0/np.pi
deg2rad=np.pi/180.0

# parse command line parameters, convert to radians
import sys
resol      = sys.argv[1]                # resolution, e.g., R80000m
lat        = deg2rad*float(sys.argv[2]) # latitude position
lon        = deg2rad*float(sys.argv[3]) # longitude position

print('-----------------------------------------')
print('Working on ICON grid with resolution', resol)
print('Searching for cell closest to lat', lat*rad2deg, 'and lon', lon*rad2deg, 'using the Haversine formulae')

# gridfile including path
gridfile = '/work/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_'+resol+'.nc'

# load lat-lon info of grid in radians
import xarray as xr
ds_grid = xr.load_dataset(gridfile)
clat = ds_grid['clat'].values
clon = ds_grid['clon'].values

dlat = np.abs(clat-lat)
dlon = np.abs(clon-lon)

# Use Haversine formulae for the distance on a sphere
# https://en.wikipedia.org/wiki/Haversine_formula
havs = np.power(np.sin(dlat/2.0),2) + np.cos(clat)*np.cos(lat)*np.power(np.sin(dlon/2.0),2)
dist = 2*np.arcsin(np.sqrt(havs)) 

print('Closest cell has index', np.argmin(dist))
print('Note: The index found here is shited by -1 relative to the ICON grid')
print('      indexing, as the latter starts with 1. Therefore, the')
print('      index found here is the value that should be used in Tricco.')
print('-----------------------------------------')
