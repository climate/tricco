# Perform benchmarking of 2d cloud data given a previously computed cubulation

# parse command line parameters
import sys
resol      = sys.argv[1]

# load other needed packages
sys.path.append("/home/b/b380459/tricco/")
import tricco
from tricco.alternatives import nwx
import datetime
import networkx as networkx
import numpy as np

print("                                         ")
print("-----------------------------------------")
print("Networkx alternative, compute and save full graph of grids for vertex and edge connectivity")
print("Working on resolution of", resol)

# grid file
gridfile = "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_"+resol+".nc"

# location where to save full graphs
graphpath = "/scratch/b/b380459/tricco_output/"

dtime_prep = list()
dtime_vert = list()
dtime_edge = list()

# prepare grid
begin_time = datetime.datetime.now()
grid = nwx.prepare_grid(model="ICON", file=gridfile)
end_time = datetime.datetime.now()
dtime_prep.append(end_time-begin_time)

# vertex connectivity
begin_time = datetime.datetime.now()
fgraph_vert = nwx.compute_fullgraph(grid, connectivity="vertex")
networkx.write_gpickle(fgraph_vert, graphpath+"/icon-grid_nawdex_78w40e23n80n_"+resol+"_fgraph_vertex.gpickle")
end_time = datetime.datetime.now()
dtime_vert.append(end_time-begin_time)

# edge connectivity
begin_time = datetime.datetime.now()
fgraph_edge = nwx.compute_fullgraph(grid, connectivity="edge")
networkx.write_gpickle(fgraph_edge, graphpath+"/icon-grid_nawdex_78w40e23n80n_"+resol+"_fgraph_edge.gpickle")
end_time = datetime.datetime.now()
dtime_edge.append(end_time-begin_time)

print(" ")
print("Time to prepare grid:")
print("Mean:", np.mean(dtime_prep), "Min:", np.min(dtime_prep), "Max:", np.max(dtime_prep))
print(" ")
print("Time to compute and store full graph for vertex connectivity")
print("Mean:", np.mean(dtime_vert), "Min:", np.min(dtime_vert), "Max:", np.max(dtime_vert))
print(" ")
print("Time to compute and store full graph for edge connectivity")
print("Mean:", np.mean(dtime_edge), "Min:", np.min(dtime_edge), "Max:", np.max(dtime_edge))

print("-----------------------------------------")
print("                                         ")
