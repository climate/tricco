# Perform benchmarking of 2d cloud data using the self-programmed breadth-first search

# parse command line parameters
import sys
resol      = sys.argv[1]

# load other needed packages
sys.path.append("/home/b/b380459/tricco/")
import tricco
from tricco.alternatives import ownbfs
import datetime
import xarray as xr
import numpy as np

print("                                         ")
print("-----------------------------------------")
print("Own BFS alternative for 2d connected component labeling")
print("Working on resolution of", resol)

# read in cloud data
# cloud file depends on resolution
datapath={"R80000m": "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-80km-mis-0001/",
          "R40000m": "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-40km-mis-0001/",
          "R20000m": "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-20km-mis-0001/",
          "R10000m": "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-10km-mis-0001/"}
datafile={"R80000m": "nawdexnwp-80km-mis-0001_2016092200_2d_30min_DOM01_ML_",
          "R40000m": "nawdexnwp-40km-mis-0001_2016092200_2d_30min_DOM01_ML_",
          "R20000m": "nawdexnwp-20km-mis-0001_2016092200_2d_30min_DOM01_ML_",
          "R10000m": "nawdexnwp-10km-mis-0001_2016092200_2d_30min_DOM01_ML_"}

dtime_read = list()
dtime_vert = list()
dtime_edge = list()
# loop over 1 day of 30-min output data --> 48 timesteps
for time in range(10,59):
    # read grid and cloud data
    begin_time = datetime.datetime.now()
    gridfile = "/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_"+resol+".nc"
    grid = ownbfs.prepare_grid(model="ICON", file=gridfile)
    filename = datapath[resol]+"/"+datafile[resol]+"00"+str(time)+".nc"
    data = xr.open_dataset(filename)["clct"].squeeze()
    end_time = datetime.datetime.now()
    dtime_read.append(end_time-begin_time)
    # perform connected component analysis for vertex connectivity
    begin_time = datetime.datetime.now()
    components_vert = ownbfs.compute_components_2d(grid, data, 85.0, connectivity="vertex")
    end_time = datetime.datetime.now()
    dtime_vert.append(end_time-begin_time)
    # perform connected component analysis for edge connectivity
    begin_time = datetime.datetime.now()
    components_edge = ownbfs.compute_components_2d(grid, data, 85.0, connectivity="edge")
    end_time = datetime.datetime.now()
    dtime_edge.append(end_time-begin_time)

print(" ")
print("Time to read 2d cloud data for 1 time step; done for 48 time steps")
print("Mean:", np.mean(dtime_read), "Min:", np.min(dtime_read), "Max:", np.max(dtime_read))
print(" ")
print("Time to do 2d connected component labeling with vertex connectivity for 1 time step; done for 48 time steps")
print("Mean:", np.mean(dtime_vert), "Min:", np.min(dtime_vert), "Max:", np.max(dtime_vert))
print(" ")
print("Time to do 2d connected component labeling with edge connectivity for 1 time step: done for 48 time steps")
print("Mean:", np.mean(dtime_edge), "Min:", np.min(dtime_edge), "Max:", np.max(dtime_edge))

print("-----------------------------------------")
print("                                         ")
