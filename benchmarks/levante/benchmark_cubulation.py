# Perform benchmarking of cubulation routine on DKRZ Levante

# parse command line parameters
import sys
resol      = sys.argv[1]
startcell  = int(sys.argv[2])
searchrad  = int(sys.argv[3])
savecubul  = int(sys.argv[4])

# gridfile including path
gridfile = '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_'+resol+'.nc'

# load other needed packages
sys.path.append('/home/b/b380459/tricco/')
import tricco
import datetime

print('                                         ')
print('-----------------------------------------')
print('Working on resolution of', resol)
print('Start cell:', startcell, 'Search radius:', searchrad, 'Save cubulation:', savecubul)

begin_time = datetime.datetime.now()
tricco.grid_functions.grid = tricco.prepare_grid(model='ICON',path='/', file=gridfile)
end_time = datetime.datetime.now()
print('Time to read the grid:', end_time-begin_time)

begin_time = datetime.datetime.now()
cubulation = tricco.compute_cubulation(start_triangle=startcell, radius=searchrad, print_progress=False)
end_time = datetime.datetime.now()
print('Time to compute cubulation:', end_time-begin_time)

# optional: save cubulation
if savecubul == 1:
    import numpy as np
    np.save('/scratch/b/b380459/tricco_output/icon-grid_nawdex_78w40e23n80n_'
            +resol+'_cubulation_start'+str(startcell)+'_radius'+str(searchrad), cubulation)

print('-----------------------------------------')
print('                                         ')
