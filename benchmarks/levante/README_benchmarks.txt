Benchmarking runtime of cubulation and connected component identification 

Tests are performed on an exclusive compute node of the DKRZ supercomputer Levante in Hamburg, Germany.

A compute node has the following specs (https://docs.dkrz.de/doc/levante/configuration.html):
  * 2x AMD 7763 CPU @ 2.45 Ghz,
  * 128 cores in total, 
  * 256 GB main memory.
 
As for the grids we consider limited-area ICON grids that cover a large part of the North Atlantic. They were for example used for the NAWDEX simulations described Senf, F., A. Voigt et al, 2020: Increasing Resolution and Resolving Convection Improve the Simulation of Cloud‐Radiative Effects Over the North Atlantic, JGR Atmospheres. https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020JD032667.

Usage:

benchmark_cubulation.run is a batch job that is submitted to an exclusive compute node via sbatch and that calls benchmark_cubulation.py. The grid resolution, start triangle and search radius for the cubulation are handed over to benchmark_cubulation.py, which reads in the grid file and does the cubulation.

In a similar manner, benchmark_2d.run and benchmark_3d.run measure the execution time for computing connected components using the tricco cubulation approach.

benchmark_alternative_networkx_2d.run and benchmark_alternative_own-bfs_2d.run measure the execution time for using the alternative approaches for 2d connected component finding. benchmark_alternative_networkx_fullgraph.run measure the time to compute and save the full graph of the grids with all possible connections between cells. 
