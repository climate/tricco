# Perform benchmarking of 3d cloud data given a previously computed cubulation

# parse command line parameters
import sys
resol      = sys.argv[1]
startcell  = int(sys.argv[2])
searchrad  = int(sys.argv[3])

# gridfile including path
gridfile = '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/grids/icon-grid_nawdex_78w40e23n80n_'+resol+'.nc'

# load other needed packages
import cc3d
sys.path.append('/home/b/b380459/tricco/')
import tricco
import datetime

print('                                         ')
print('-----------------------------------------')
print('Working on resolution of', resol)
print('Start cell:', startcell, 'Search radius:', searchrad)

# load previously computed cubulation
import numpy as np
cubulpath = '/scratch/b/b380459/tricco_output/'
cubulfile = cubulpath+'/icon-grid_nawdex_78w40e23n80n_'+resol+'_cubulation_start'+str(startcell)+'_radius'+str(searchrad)+'.npy'
cubulation = np.load(cubulfile, allow_pickle=True)

# read in cloud data

# cloud file depends on resolution
datapath={'R80000m': '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-80km-mis-0001/',
          'R40000m': '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-40km-mis-0001/',
          'R20000m': '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-20km-mis-0001/',
          'R10000m': '/work/bb1018/from_Mistral/bb1018/b380459/NAWDEX/ICON_OUTPUT_NWP/nawdexnwp-10km-mis-0001/'}
datafile={'R80000m': 'nawdexnwp-80km-mis-0001_2016092200_3dcloud_DOM01_ML_',
          'R40000m': 'nawdexnwp-40km-mis-0001_2016092200_3dcloud_DOM01_ML_',
          'R20000m': 'nawdexnwp-20km-mis-0001_2016092200_3dcloud_DOM01_ML_',
          'R10000m': 'nawdexnwp-10km-mis-0001_2016092200_3dcloud_DOM01_ML_'}

dtime_dat = list()
dtime_ver = list()
dtime_edg = list()
# loop over 2 days of 1-hour output data --> 48 timesteps
for time in range(10,59):
    # read in data
    begin_time = datetime.datetime.now()
    field, field_cube = tricco.prepare_field_lev(model='ICON', path=datapath[resol], 
                            file=datafile[resol]+'00'+str(time)+'.nc',
                            var='clc', threshold=85.0, cubulation=cubulation)
    end_time = datetime.datetime.now()
    dtime_dat.append(end_time-begin_time)
    # perform connected component analysis for vertex connectivity
    begin_time = datetime.datetime.now()
    _ = tricco.compute_connected_components_3d(cubulation, field_cube, connectivity = 'vertex')
    end_time = datetime.datetime.now()
    dtime_ver.append(end_time-begin_time)
    # perform connected component analysis for edge connectivity
    begin_time = datetime.datetime.now()
    _ = tricco.compute_connected_components_3d(cubulation, field_cube, connectivity = 'edge')
    end_time = datetime.datetime.now()
    dtime_edg.append(end_time-begin_time)

print(' ')
print('Time to read in 1 time step of data, including moving it onto the 3d x lev cubulated grid; done for 48 time steps')
print('Mean:', np.mean(dtime_dat), 'Min:', np.min(dtime_dat), 'Max:', np.max(dtime_dat))
print(' ')
print('Time to do 3d connected component labeling with vertex connectivity for 1 time step; done for 48 time steps')
print('Mean:', np.mean(dtime_ver), 'Min:', np.min(dtime_ver), 'Max:', np.max(dtime_ver))
print(' ')
print('Time to do 3d connected component labeling with edge connectivity for 1 time step; done for 48 time steps')
print('Mean:', np.mean(dtime_edg), 'Min:', np.min(dtime_edg), 'Max:', np.max(dtime_edg))

print('-----------------------------------------')
print('                                         ')
